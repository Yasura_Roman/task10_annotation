package com.yasiuraroman.annotation;

import java.util.Arrays;

public class ClassWithAnnotation {

    @MyFieldAnnotation( name = "field1", type = "String")
    private String field1;

    private int field2 = 8;

    @MyFieldAnnotation( name = "field3", type = "Integer")
    private Integer field3;

    private String field4;

    private Object unknownObject;

    public int methodReturnOne(){
        return 1;
    }

    public void methodSetField4Value(String value){
        field4 = value;
    }

    public int powTo(int value, int power){
        return (int) Math.pow(value,power);
    }

    public String myMethod(String a, int... args) {
        return "Invoking myMethod(String, int[]) with params:" + a + Arrays.toString(args);
    }

    public String myMethod(String... args) {
        return "Invoking myMethod(String[]) with params:" + Arrays.toString(args);
    }

    public int getField2() {
        return field2;
    }

}

package com.yasiuraroman.reflection;

import com.yasiuraroman.annotation.ClassWithAnnotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import com.yasiuraroman.annotation.MyFieldAnnotation;
import org.apache.logging.log4j.Logger;

public class MyReflection {
    private Class testClass;
    private ClassWithAnnotation annotationClass;
    private Logger logger;

    public MyReflection(Logger logger) {
        annotationClass =  new ClassWithAnnotation();
        testClass = annotationClass.getClass();
        this.logger = logger;
    }

    private Field[] allField(){
        return testClass.getDeclaredFields();
    }

    public void printAllField(){
        for (Field field: allField()
             ) {
        logger.info("Field name = " + field.getName()
                + ", type = " + field.getType()
                + ", modifiers = " + field.getModifiers()
                + ", annotation = " + field.getDeclaredAnnotations()
        );
        }
    }

    public void printFieldWithAnnotation(){
        String annot;
        for (Field field: allField()
        ) {
            annot = fieldHasMyFieldAnnotation(field);
            if (!annot.isEmpty()){
                logger.info(field.getName() + "  "
                        + annot
                );
            }
        }
    }

    private String fieldHasMyFieldAnnotation(Field field){
        if (field.isAnnotationPresent(MyFieldAnnotation.class)){
            MyFieldAnnotation myFieldAnnotation = (MyFieldAnnotation) field.getAnnotation(MyFieldAnnotation.class);
            StringBuilder result = new StringBuilder();
            result.append("Annotation MyFieldAnnotation : name = ");
            result.append(myFieldAnnotation.name());
            result.append(", type = ");
            result.append(myFieldAnnotation.type());
            return result.toString();
        }
        return "";
    }

    public void invokeMethods() {
        try {
            Method method1 = testClass.getDeclaredMethod("methodReturnOne");
            Method method2 = testClass.getDeclaredMethod("methodSetField4Value",String.class);
            Method method3 = testClass.getDeclaredMethod("powTo",int.class,int.class);

            logger.info("method 1 return int 1");
            int result1 = (int) method1.invoke(annotationClass);
            logger.info("method1 result = "+result1);

            logger.info("method 2 return void params String");
            method2.invoke(annotationClass,"newValue");

            logger.info("method 3 return int pow from value (8) params int value =2, int pow = 3 ");
            int result3 = (int) method3.invoke(annotationClass,2,3);
            logger.info("method3 result = "+result3);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException |InvocationTargetException e) {
            logger.error(e);
        }

    }

    public void setFieldValue() {
        try {
            Field field = testClass.getDeclaredField("field2");
            logger.info("field value = " + annotationClass.getField2());
            field.setAccessible(true);
            if (field.getType() == int.class){
                field.setInt(annotationClass,2);
            }
            logger.info("setting field value 2 ");
            logger.info("field value = " + annotationClass.getField2());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            logger.error(e);
        }
    }

    public void invokeVarArg () {
        try {
            Method method1 = testClass.getMethod("myMethod", String.class, int[].class);
            Method method2 = testClass.getMethod("myMethod", String[].class);

            String result1 = (String) method1.invoke(annotationClass, "string", new int[] {1, 2});
            logger.info(result1);

            String result2 = (String) method2.invoke(annotationClass, new Object[] {new String[]{"first", "second", "third"}});
            logger.info(result2);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }
    }

    public void showInformationAboutUnknownClass() {
        try {
            Class clazz = testClass.getDeclaredField("unknownObject").getType().getClass();
            logger.info("---------------FIELDS--------------------------");
            Arrays.asList(clazz.getDeclaredFields()).forEach(f -> logger.info(f.toString()));
            logger.info("---------------CONSTRUCTORS--------------------------");
            Arrays.asList(clazz.getDeclaredConstructors()).forEach(c -> logger.info(c.toString()));
            logger.info("---------------ANNOTATIONS--------------------------");
            Arrays.asList(clazz.getDeclaredAnnotations()).forEach(a -> logger.info(a.toString()));
            logger.info("---------------METHODS--------------------------");
            Arrays.asList(clazz.getDeclaredMethods()).forEach(m -> logger.info(m.toString()));
        } catch (NoSuchFieldException e) {
            logger.error("error");
            e.printStackTrace();
        }
    }
}

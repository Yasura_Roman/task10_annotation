package com.yasiuraroman.controller;

import com.yasiuraroman.reflection.MyReflection;
import org.apache.logging.log4j.Logger;

public class ReflectionControllerImp implements ReflectionController {

    private MyReflection model;

    public ReflectionControllerImp(Logger logger) {
        model = new MyReflection(logger);
    }

    @Override
    public void field() {
        model.printAllField();
    }

    @Override
    public void annotationValue() {
        model.printFieldWithAnnotation();
    }

    @Override
    public void invokeMethod() {
        model.invokeMethods();
    }

    @Override
    public void setValueToField() {
        model.setFieldValue();
    }

    @Override
    public void invokeMethodWithVarArg() {
        model.invokeVarArg();
    }

    @Override
    public void classInfo() {
        model.showInformationAboutUnknownClass();
    }
}

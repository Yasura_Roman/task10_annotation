package com.yasiuraroman.controller;

public interface ReflectionController {
    void field();
    void annotationValue();
    void invokeMethod();
    void setValueToField();
    void invokeMethodWithVarArg();
    void classInfo();
}

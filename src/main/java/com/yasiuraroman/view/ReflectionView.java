package com.yasiuraroman.view;

import com.yasiuraroman.controller.ReflectionController;
import com.yasiuraroman.controller.ReflectionControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ReflectionView {

    private ReflectionController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public ReflectionView() {
        controller = new ReflectionControllerImp(logger) {
        };
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - field");
        menu.put("2", "  2 - annotation value");
        menu.put("3", "  3 - invoke method");
        menu.put("4", "  4 - set value to field");
        menu.put("5", "  5 - invoke method with var arg");
        menu.put("6", "  6 - class info");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", controller::field);
        methodsMenu.put("2", controller::annotationValue);
        methodsMenu.put("3", controller::invokeMethod);
        methodsMenu.put("4", controller::setValueToField);
        methodsMenu.put("5", controller::invokeMethodWithVarArg);
        methodsMenu.put("6", controller::classInfo);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

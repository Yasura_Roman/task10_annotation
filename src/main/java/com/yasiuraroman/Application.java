package com.yasiuraroman;

import com.yasiuraroman.view.ReflectionView;

public class Application {
    public static void main(String[] args) {
        ReflectionView view = new ReflectionView();
        view.show();
    }
}
